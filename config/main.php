<?php

use Core\Logging\LoggerFactory;
use Core\Routing\RouterFactory;

return [
    'components' => [
        RouterFactory::KEY_INSTANCE => [
            'factory' => RouterFactory::class,
            'file' => $_SERVER['DOCUMENT_ROOT'] . '/../app/Http/routes.php',
        ],
        'logger' => [
            'factory' => LoggerFactory::class,
        ]

        /*'router' => [
            //'class' => \Core\Routing\UrlRouter::class,
            'class' => \Core\Routing\Router::class,
            'file' => $_SERVER['DOCUMENT_ROOT'] . '/../app/Http/routes.php',
        ],
        'logger' => [
            'class' => \Core\Logging\Logger::class,
            'writer' => new \Core\Logging\FileWriter(),
            'formatter' => new \Core\Logging\StringFormatter(),
        ],*/
    ],
    // ..
];