<?php


namespace App\Http\Controllers;
/**
 * контроллер PageController
 */

class PageController
{
    public function viewAction($id)
    {
        echo 'Page view with id ' . $id;
    }
}