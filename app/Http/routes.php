<?php

/**
 * @var \Core\Routing\Router $this
 */

$this->get('/products/test', function(){
    echo 'Im am closure Products';
});


$this->get('/products/show', [new \App\Http\Controllers\ProductsController(), 'show']);

$this->get('/', [new \App\Http\Controllers\IndexController(), 'indexAction']);