<?php

/**
 * индексный файл
 */
declare(strict_types=1);

use Core\Application;

ini_set('display_errors', '1');

include '../vendor/autoload.php';

$config = include '../config/main.php';

$app = Application::getInstance();
$app->configure($config);

$app->run();



$router = $app->get('router');


exit();