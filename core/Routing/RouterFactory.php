<?php


namespace Core\Routing;


use Core\Application;
use Core\Contracts\ComponentAbstract;
use Core\Contracts\ComponentFactoryAbstract;

class RouterFactory extends ComponentFactoryAbstract
{
    public const KEY_INSTANCE = 'router';
    public const KEY_FILE = 'file';

    public function createConcreteInstance(): ComponentAbstract
    {
        $config = $this->app->config(Application::KEY_COMPONENTS)[self::KEY_INSTANCE];
        return new Router($config[self::KEY_FILE]);
    }
}