<?php


namespace Core\Routing;


class UrlRouter implements RouterInterface
{

    public function route()
    {
        $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        $parts = explode('/', $url);

        $controllerName = 'App\\Http\\Controllers\\IndexController';
        if (!empty($parts[1])) {
            $controllerName = 'App\\Http\\Controllers\\' . ucfirst($parts[1]) . 'Controller';
            if (!class_exists($controllerName)) {
                return false;
            }
        }

        $controller = new $controllerName();

        $action = 'indexAction';
        if (!empty($parts[2])) {
            $action = $parts[2] . 'Action';
            if (!method_exists($controller, $action)) {
                return false;
            }
        }

        $params = [];
        for ($i = 3; $i < count($parts); $i++) {
            if ($i % 2 != 0) {
                $keys[] = $parts[$i];
            } else {
                $values[] = $parts[$i];
            }
        }

        if (!empty($keys)) {
            if (count($keys) > count($values)) {
                $values[] = '';
            }
            $params = array_combine($keys, $values);
        }

        return [$controller, $action];
    }
}