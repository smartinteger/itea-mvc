<?php


namespace Core\Routing;


use core\Application;
use Core\Contracts\BootstrapInterface;
use Core\Contracts\ComponentAbstract;

class Router extends ComponentAbstract implements BootstrapInterface, RouterInterface
{
    public const METHOD_POST = 'POST';
    public const METHOD_GET = 'GET';

    protected $routes = [];

    protected $get = [];

    protected $post = [];

    protected $file;

    public function __construct($file)
    {
        //$this->file = $params['file'] ?? null;
        $this->file = $file;

        $this->bootstrap();
    }

    /**
     * @return callable|null
     */
    public function route()
    {
        $currentPath = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $currentMethod = $_SERVER['REQUEST_METHOD'];

        $this->loadGet();

        $this->loadPost();

        if (isset($this->routes[$currentMethod][$currentPath])) {
            return $this->routes[$currentMethod][$currentPath];
        }

        return null;
    }

    protected function loadGet()
    {
        $this->get = $_GET;
    }

    public function getParams()
    {
        return $this->get;
    }

    protected function loadPost()
    {
        $this->post = $_POST;
    }

    public function postParams()
    {
        $this->post;
    }

    public function bootstrap()
    {
        /*if (!file_exists($this->file)) {
            throw new \Exception('Routes file not found');
        }*/

        include $this->file;
    }

    public function get(string $url, callable $action)
    {
        $this->addRoute(self::METHOD_GET, $url, $action);
    }

    public function post(string $url, callable $action)
    {
        $this->addRoute(self::METHOD_POST, $url, $action);
    }

    public function addRoute(string $method, string $url, callable $action)
    {
        $this->routes[$method][$url] = $action;
    }
}