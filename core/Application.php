<?php
/**
 * Файл для того-то и для того-то
 *
 * @package core
 * @version 1.0
 * @author Yurii Orlyk
 * @link https://bitbucket.org/iteaphp/mvc-project/src/master/core/Application.php
 */

namespace Core;

//use Core\Router;
use Core\Contracts\BootstrapInterface;
use Core\Contracts\ComponentAbstract;
use Core\Contracts\ContainerAbstract;
use Core\Contracts\ContainerInterface;
use Core\Contracts\RunnableInterface;

/**
 * Class Application
 *
 * Данный класс реализует интерфейс ContainerInterface.
 * Он выступает сервис-контейнером для компонентов системы
 *
 * @package Core
 */
class Application extends ContainerAbstract implements RunnableInterface, BootstrapInterface
{
    /**
     * @var Application Инстанс текущего синглтона
     */
    protected static $instance;

    private function __construct()
    {}

    private function __clone()
    {}

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @throws \ReflectionException
     * @see Router::route()
     */
    public function run()
    {
        if ($this->has('router')) {
            $action = $this->get('router')->route();

            if ($action) {
                $params = $this->get('router')->getParams();
                $this->runAction($action, $params);
            }
        }

        $this->logger->debug('Test message');
    }

    /**
     * @param callable $action Обьект который нужно вызвать по запросу
     * @param array $params Массив параметров полученных с HTTP запроса
     * @return mixed
     * @throws \ReflectionException
     */
    protected function runAction($action, $params = [])
    {
        $reflectionMethod = new \ReflectionMethod($action[0], $action[1]);
        $dependencies = [];
        foreach ($reflectionMethod->getParameters() as $parameter) {
            $name = $parameter->getName();
            if (array_key_exists($name, $params)) {
                $dependencies[$name] = $params[$name];
            }
        }

        return call_user_func_array($action, $dependencies);
    }
}