<?php


namespace Core\Database;

/**
 * Class QueryBuilder
 * @package Core\Database
 *
 * SELECT * FROM table
 */
class QueryBuilder implements QueryBuilderInterface
{
    protected $db;

    protected $table;
    protected $column = ['*'];
    protected $where = [];
    protected $limit = '';
    protected $offset = '';
    protected $order = [];

    public function __construct(Db $db)
    {
        $this->db = $db;
    }

    public function select($columns): QueryBuilderInterface
    {
        $this->column = (array) $columns;

        return $this;
    }

    public function where($conditions): QueryBuilderInterface
    {
        $this->where = $conditions;

        return $this;
    }

    public function table($table): QueryBuilderInterface
    {
        $this->table = $table;

        return $this;
    }

    public function limit($limit): QueryBuilderInterface
    {
        $this->limit = $limit;

        return $this;
    }

    public function offset($offset): QueryBuilderInterface
    {
        $this->offset = $offset;

        return $this;
    }

    public function order($order): QueryBuilderInterface
    {
        $this->order = $order;

        return $this;
    }

    public function build(): string
    {
        if (empty($this->table)) {
            throw new \Exception('table is required');
        }

        $result = "SELECT " . implode(', ', $this->column);

        $result .= " FROM " . $this->table;

        if ($this->where) {
            $result .= " WHERE ";
            foreach ($this->where as $key => $value) {
                $where[] = $key . " = " . $value;
            }
            $result .= implode(" AND ", $where);
        }

        if ($this->order) {

            $result .= " ORDER BY ";

            foreach ($this->order as $key => $value) {
                $order[] = $key . " " . $value;
            }
            $result .= implode(", ", $order);
        }

        if ($this->limit) {
            $result .= " LIMIT " . $this->limit;

            if ($this->offset) {
                $result .= " OFFSET " . $this->offset;
            }
        }

        $this->reset();

        return $result;
    }

    public function one(): ?array
    {
        $this->limit = 1;
        $sql = $this->build();
        $response = $this->db->query($sql);

        return mysqli_fetch_assoc($response);
    }

    public function all(): ?array
    {
        $sql = $this->build();
        $response = $this->db->query($sql);

        return mysqli_fetch_all($response, MYSQLI_ASSOC);
    }

    public function reset()
    {
        $this->table = null;
        $this->column = ['*'];
        $this->where = [];
        $this->limit = '';
        $this->offset = '';
        $this->order = [];
    }
}