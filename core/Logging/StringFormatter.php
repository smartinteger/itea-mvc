<?php


namespace Core\Logging;


class StringFormatter implements FormatterInterface
{
    protected $format = '[{date}] {level}: {message} {context}';

    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    public function format($level, $message, $context = [])
    {
        $output = $this->format;
        $output = str_replace('{date}', date('Y-m-d H:i:s'), $output);
        $output = str_replace('{level}', $level, $output);
        $output = str_replace('{message}', $message, $output);
        $output = str_replace('{context}', json_encode($context), $output);

        return $output;
    }
}