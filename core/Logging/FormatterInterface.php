<?php


namespace Core\Logging;


interface FormatterInterface
{
    public function format($level, $message, $context = []);
}