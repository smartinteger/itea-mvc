<?php

namespace Core\Logging;


use Core\Contracts\ComponentAbstract;
use Psr\Log\LogLevel;


/**
 * Class Logger
 * @package Core\Logging
 *
 * @todo Сделать конструктор с рефлексией
 */
class Logger extends ComponentAbstract implements \Psr\Log\LoggerInterface
{
    protected $writer;

    protected $formatter;

    //protected $adapter;

    public function __construct(WriterInterface $writer, FormatterInterface $formatter)
    {
        $this->writer = $writer;
        $this->formatter = $formatter;

        //$this->adapter = new Adapter();
    }

    /**
     * Logger constructor.
     * Это временный конструктор, пока не сделали dependency injection
     *
     * @param $params
     *
     * @todo Сделать передачу параметров через рефлексию
     */
    /*public function __construct($params)
    {
        $this->writer = $params['writer'];
        $this->formatter = $params['formatter'];
    }*/

    /**
     * @inheritDoc
     */
    public function emergency($message, array $context = array())
    {
        $this->log(LogLevel::EMERGENCY, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function alert($message, array $context = array())
    {
        $this->log(LogLevel::ALERT, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function critical($message, array $context = array())
    {
        $this->log(LogLevel::CRITICAL, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function error($message, array $context = array())
    {
        $this->log(LogLevel::ERROR, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function warning($message, array $context = array())
    {
        $this->log(LogLevel::WARNING, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function notice($message, array $context = array())
    {
        $this->log(LogLevel::NOTICE, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function info($message, array $context = array())
    {
        $this->log(LogLevel::INFO, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function debug($message, array $context = array())
    {
        $this->log(LogLevel::DEBUG, $message, $context);
    }

    /**
     * @inheritDoc
     */
    public function log($level, $message, array $context = array())
    {
        $data = $this->formatter->format($level, $message, $context);
        $this->writer->write($data);
    }

    public function bootstrap()
    {
        // TODO: Implement bootstrap() method.
    }
}