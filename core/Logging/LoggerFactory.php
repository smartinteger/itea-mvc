<?php


namespace Core\Logging;


use Core\Contracts\ComponentAbstract;
use Core\Contracts\ComponentFactoryAbstract;

class LoggerFactory extends ComponentFactoryAbstract
{
    public function createConcreteInstance(): ComponentAbstract
    {
        $writer = new FileWriter();
        $formatter = new StringFormatter();

        return new Logger($writer, $formatter);
    }
}