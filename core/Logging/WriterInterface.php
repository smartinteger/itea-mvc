<?php


namespace Core\Logging;


interface WriterInterface
{
    public function write($data);
}