<?php


namespace Core\Contracts;


abstract class ComponentAbstract implements BootstrapInterface
{
    abstract public function bootstrap();
}