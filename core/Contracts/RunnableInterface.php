<?php


namespace Core\Contracts;


interface RunnableInterface
{
    public function run();
}