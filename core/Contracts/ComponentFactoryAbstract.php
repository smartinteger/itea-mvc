<?php


namespace Core\Contracts;


abstract class ComponentFactoryAbstract
{
    protected $app;

    public function __construct(ContainerInterface $app)
    {
        $this->app = $app;
    }

    public function createInstance(): ComponentAbstract
    {
        return $this->createConcreteInstance();
    }

    abstract public function createConcreteInstance(): ComponentAbstract;
}